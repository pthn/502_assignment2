<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Device;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image_url');
            $table->string('brand');
            $table->string('serial_no')->nullable()->unique();
            $table->string('description')->nullable();
            $table->string('operating_system');
            $table->string('display_size');
            $table->string('ram');
            $table->string('usb_ports');
            $table->string('hdmi_ports');
            $table->decimal('rental_price');
            $table->foreignId('status_id')->references('id')->on('device_statuses');
            $table->foreignId('created_by')->nullable()->references('id')->on('users');
            $table->foreignId('updated_by')->nullable()->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('devices');
        Schema::enableForeignKeyConstraints();

        /*Device::truncate();*/
    }
};
