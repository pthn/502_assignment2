<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Rental;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rentals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->decimal('rent_period');
            $table->timestamp('return_date')->nullable();
            $table->decimal('rented_price');
            $table->decimal('insurance')->nullable();
            $table->decimal('deposit');
            $table->decimal('discount')->nullable();
            $table->string('comment')->nullable();
            $table->foreignId('rental_status')->references('id')->on('rental_statuses');
            $table->foreignId('rented_by')->references('id')->on('users');
            $table->foreignId('updated_by')->nullable()->references('id')->on('users');
            $table->foreignId('device_id')->references('id')->on('devices');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('rentals');
        Schema::enableForeignKeyConstraints();
    }
};
