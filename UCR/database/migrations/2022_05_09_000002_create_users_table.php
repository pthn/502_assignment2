<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('f_name');
            $table->string('l_name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('phone_number')->unique();
            $table->string('password');
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('blacklist_count')->nullable();
            $table->foreignId('updated_by')->nullable()->references('id')->on('users');
            $table->foreignId('role_id')->references('id')->on('user_roles');
            $table->rememberToken();
            $table->timestamps();
            /** ->onDelete('cascade'); if want to delete the children when parent is deleted */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*Schema::table('users', function($table)
        {
            $table->dropForeign('updated_by');
            $table->dropForeign('role_id');
        });
        User::truncate();
        */
        
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('users');
        Schema::enableForeignKeyConstraints();
    }
};
