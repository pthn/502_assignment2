<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Database\Seeders\DeviceStatusSeeder;
use Database\Seeders\RentalStatusSeeder;
use Database\Seeders\DeviceSeeder;
use Database\Seeders\RentalSeeder;
use Database\Seeders\UserRoleSeeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\AccountSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DeviceStatusSeeder::class);
        $this->call(RentalStatusSeeder::class);
        $this->call(UserRoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(AccountSeeder::class);
        $this->call(DeviceSeeder::class);
        $this->call(RentalSeeder::class);
    }
}
