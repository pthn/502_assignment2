<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use DB;
use App\Models\UserRole;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserRole::truncate();

        $user_roles = Config::get('user.roles');
        foreach ($user_roles as $role)
        {
            DB::table('user_roles')->insert(
                ['name' => $role]            
            );
        }
    }
}
