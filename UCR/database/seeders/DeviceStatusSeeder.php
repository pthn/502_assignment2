<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use DB;
use App\Models\DeviceStatus;

class DeviceStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DeviceStatus::truncate();
        
        $device_statuses = Config::get('computer.statuses');
        foreach ($device_statuses as $status)
        {
            DB::table('device_statuses')->insert(
                ['name' => $status]            
            );
        }
    }
}
