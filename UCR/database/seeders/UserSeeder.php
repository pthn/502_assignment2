<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $users =  [
            [
                'f_name' => 'Student',
                'l_name' => 'A',
                'email' => 's.a@utas.edu.au',
                'phone_number' => '023456789',
                'password' => '$2y$10$PPfPSxSShFgs5nMCvmVXROIBXkAeWxG5Jba20nYUl.7P.035mhjry', // pass123456
                'status' => 1,
                'role_id' => 1 // STUDENT A
            ],
            [
                'f_name' => 'Student',
                'l_name' => 'B',
                'email' => 's.b@utas.edu.au',
                'phone_number' => '123456789',
                'password' => '$2y$10$PPfPSxSShFgs5nMCvmVXROIBXkAeWxG5Jba20nYUl.7P.035mhjry',
                'status' => 1,
                'role_id' => 1 // STUDENT B
            ],
            [
                'f_name' => 'Staff',
                'l_name' => 'A',
                'email' => 'st.a@utas.edu.au',
                'phone_number' => '223456789',
                'password' => '$2y$10$PPfPSxSShFgs5nMCvmVXROIBXkAeWxG5Jba20nYUl.7P.035mhjry',
                'status' => 1,
                'role_id' => 2 // STAFF
            ],
            [
                'f_name' => 'UCR Staff',
                'l_name' => 'A',
                'email' => 'ucr.st.a@utas.edu.au',
                'phone_number' => '323456789',
                'password' => '$2y$10$PPfPSxSShFgs5nMCvmVXROIBXkAeWxG5Jba20nYUl.7P.035mhjry',
                'status' => 1,
                'role_id' => 3 // UCR_STAFF
            ],
            [
                'f_name' => 'UCR Manager',
                'l_name' => 'A',
                'email' => 'ucr.m.a@utas.edu.au',
                'phone_number' => '423456789',
                'password' => '$2y$10$PPfPSxSShFgs5nMCvmVXROIBXkAeWxG5Jba20nYUl.7P.035mhjry',
                'status' => 1,
                'role_id' => 4 // UCR_MANAGER
            ]
        ];

        DB::table('users')->insert($users);
    }
}
