<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use DB;
use App\Models\Rental;

class RentalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rental::truncate();
        $rentals =  [
            [
                'start_date' => now(),
                'end_date' => now(),
                'rent_period' => '1.5',
                'rented_price' => '14',
                'deposit' => '20',
                'rental_status' => '1',
                'rented_by' => '1', // STUDENT A
                'device_id' => '4'
            ],
            [
                'start_date' => now(),
                'end_date' => now(),
                'rent_period' => '1.5',
                'rented_price' => '15',
                'deposit' => '20',
                'rental_status' => '1',
                'rented_by' => '1', // STUDENT A
                'device_id' => '5'
            ],
            [
                'start_date' => now(),
                'end_date' => now(),
                'rent_period' => '1.5',
                'rented_price' => '15',
                'deposit' => '20',
                'rental_status' => '1',
                'rented_by' => '2', // STUDENT B
                'device_id' => '5'
            ],
            [
                'start_date' => now(),
                'end_date' => now(),
                'rent_period' => '1.5',
                'rented_price' => '14',
                'deposit' => '20',
                'rental_status' => '1',
                'rented_by' => '3', // STAFF A
                'device_id' => '4'
            ],
            [
                'start_date' => now(),
                'end_date' => now(),
                'rent_period' => '1.5',
                'rented_price' => '16',
                'deposit' => '20',
                'rental_status' => '1',
                'rented_by' => '3', // STAFF A
                'device_id' => '6'
            ],
        ];

        DB::table('rentals')->insert($rentals);
    }
}
