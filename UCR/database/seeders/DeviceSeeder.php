<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use DB;
use App\Models\Device;

class DeviceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Device::truncate();
        $devices =  [
            [
                'image_url' => 'img/dell/pexels-vlad-bagacian-3987066.jpg',
                'brand' => 'DELL',
                'serial_no' => 'D1',
                'description' => 'D1 DESC',
                'operating_system' => 'Windows 10',
                'display_size' => '15 inches',
                'ram' => '256',
                'usb_ports' => '1',
                'hdmi_ports' => '2',
                'rental_price' => '10.5',
                'status_id' => '1' // NEW
            ],
            [
                'image_url' => 'img/macbook_pro/pexels-craig-dennis-205421.jpg',
                'brand' => 'MACBOOK PRO',
                'serial_no' => 'MP1',
                'description' => 'MP1 DESC',
                'operating_system' => 'Mac OS',
                'display_size' => '17 inches',
                'ram' => '512',
                'usb_ports' => '2',
                'hdmi_ports' => '2',
                'rental_price' => '15.5',
                'status_id' => '1' // NEW
            ],
            [
                'image_url' => 'img/acer/pexels-fernando-arcos-169484.jpg',
                'brand' => 'ACER',
                'serial_no' => 'A1',
                'description' => 'A1 DESC',
                'operating_system' => 'Windows 10',
                'display_size' => '14 inches',
                'ram' => '512',
                'usb_ports' => '2',
                'hdmi_ports' => '2',
                'rental_price' => '10.5',
                'status_id' => '1' // NEW
            ],
            [
                'image_url' => 'img/dell/pexels-lisa-fotios-1266982.jpg',
                'brand' => 'DELL',
                'serial_no' => 'D2',
                'description' => 'D2 DESC',
                'operating_system' => 'Windows 11',
                'display_size' => '15 inches',
                'ram' => '256',
                'usb_ports' => '1',
                'hdmi_ports' => '2',
                'rental_price' => '10.5',
                'status_id' => '2' // RENTED
            ],
            [
                'image_url' => 'img/lenovo/pexels-olenka-sergienko-3550482.jpg',
                'brand' => 'LENOVO',
                'serial_no' => 'L1',
                'description' => 'L1 DESC',
                'operating_system' => 'Windows 10',
                'display_size' => '14 inches',
                'ram' => '512',
                'usb_ports' => '2',
                'hdmi_ports' => '2',
                'rental_price' => '11.5',
                'status_id' => '2' // RENTED
            ],
            [
                'image_url' => 'img/macbook_air/pexels-jeandaniel-francoeur-4006151.jpg',
                'brand' => 'MACBOOK AIR',
                'serial_no' => 'MA1',
                'description' => 'MA1 DESC',
                'operating_system' => 'Mac OS',
                'display_size' => '17 inches',
                'ram' => '512',
                'usb_ports' => '2',
                'hdmi_ports' => '2',
                'rental_price' => '13.5',
                'status_id' => '2' // RENTED
            ]
        ];

        DB::table('devices')->insert($devices);
    }
}