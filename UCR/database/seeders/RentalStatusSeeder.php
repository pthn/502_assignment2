<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use DB;
use App\Models\RentalStatus;

class RentalStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RentalStatus::truncate();

        $rental_statuses = Config::get('rental.statuses');
        foreach ($rental_statuses as $status)
        {
            DB::table('rental_statuses')->insert(
                ['name' => $status]            
            );
        }
    }
}
