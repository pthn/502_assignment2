<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use DB;
use App\Models\Account;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Account::truncate();
        $accounts =  [
            [
                'account_no' => '111022033044',
                'balance' => '100.00',
                'user_id' => '1' // STUDENT A
            ],
            [
                'account_no' => '211022033044',
                'balance' => '0',
                'user_id' => '2' // STUDENT B
            ],
            [
                'account_no' => '311022033044',
                'balance' => '200.00',
                'user_id' => '3' // STAFF A
            ]
        ];
        
        DB::table('accounts')->insert($accounts);
    }
}
