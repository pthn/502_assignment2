<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     * Initialize the Default Application's Configurations where there's no features to add these.
     * @return void
     */
    public function boot()
    {
        $brands = collect(['ACER', 'DELL', 'LENOVO', 'MACBOOK AIR', 'MACBOOK PRO']);
        $systems = collect(['Windows 11', 'Windows 10', 'Windows 8', 'Mac OS', 'Ubuntu', 'Chrome OS']); 
        $device_statuses = collect(['NEW', 'RENTED', 'RETURNED', 'MINOR DAMAGED', 'MAJOR DAMAGED', 'INACTIVE']);

        Config::set(['computer' => ['brands' => $brands, 'systems' => $systems, 'statuses' => $device_statuses]]);

        $rental_hours = collect(['1', '1.5', '2', '2.5', '3', '3.5', '4', '4.5', '5']);
        $rental_statuses = collect(['RENTED', 'RETURNED', 'MINOR DAMAGED', 'MAJOR DAMAGED']);
        Config::set(['rental' => ['periods' => $rental_hours, 'statuses' => $rental_statuses]]);

        $user_roles = collect(['STUDENT', 'STAFF', 'UCR_STAFF', 'UCR_MANAGER']);
        Config::set(['user' => ['roles' => $user_roles]]);
        Config::set(['rental' => ['deposit' => 50, 'insurance' => 10]]);

        // set default time zone
        date_default_timezone_set('Australia/Melbourne');
        
        Paginator::useBootstrapFive();
    }
}
