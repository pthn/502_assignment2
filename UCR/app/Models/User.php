<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'f_name',
        'l_name',
        'phone_number',
        'email',
        'password',
        'role_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function account()
    {
        return $this->hasOne(Account::class, 'user_id', 'id');
    }
    
    public function rentals()
    {
        return $this->hasMany(Rental::class, 'rented_by', 'id');
    }

    public function roles() {
        return $this->belongsToMany(UserRole::class, 'users', 'id', 'role_id');
    }

    public function hasRole(string $role_name): bool
    {
        $roles = $this->roles()->get();
        if(null !== $roles && !$roles->isEmpty())
        {
            $user_role = $roles[0]->name; // currently, only support 1 user has 1 role.
            return $user_role === $role_name;
        }
        return false;
    }

    public function isUCRStaff(): bool
    {
        return $this->hasRole('UCR_STAFF');
    }
    
    public function isUCRManager(): bool
    {
        return $this->hasRole('UCR_MANAGER');
    }

    public function isStudent(): bool
    {
        return $this->hasRole('STUDENT');
    }
       
    public function isCustomer(): bool
    {
        return (!$this->isUCRManager());
    }
    
    public function isUCR(): bool
    {
        return ($this->isUCRStaff() || $this->isUCRManager());
    }

}
