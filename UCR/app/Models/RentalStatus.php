<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RentalStatus extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    /**
    * @var array
    */
    protected $statuses = [
        'RENTED', 
        'RETURNED', 
        'MINOR DAMAGED', 
        'MAJOR DAMAGED'
    ];

   /**
    * @param int $value
    * @return string|null
    */
    public function getRentalStatusAttribute($value)
    {
        return Arr::get($this->statuses, $value);
    }
}
