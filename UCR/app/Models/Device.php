<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    public $fillable = [
        'image_url',
        'brand',
        'serial_no',
        'description',
        'operating_system',
        'display_size',
        'ram',
        'usb_ports',
        'hdmi_ports',
        'rental_price',
        'status_id'
    ];

    /**
     * Get the Device Status of this device
     */
    public function device_status()
    {
        return $this->belongsTo(DeviceStatus::class, 'status_id');
    }

    
    /**
     * Get all Device Statuses that devices have
     */
    public function device_statuses()
    {
        return $this->hasMany(DeviceStatus::class, 'id', 'status_id');
    }

    /**
     * @return HasMany
     * @description get all rental for the device
     */
    public function rentals()
    {
        return $this->hasMany(Rental::class);
    }

    /**
     * @return HasOne
     * @description get the latest rental for each device
     */
    public function rental()
    {
        return $this->hasOne(Rental::class, 'device_id', 'id')->latestOfMany();
        
    }
}
