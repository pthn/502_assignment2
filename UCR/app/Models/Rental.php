<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rental extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    /**
     * @return BelongsTo
     * @description get the device for the rental.
     */
    public function device()
    {
        return $this->belongsTo(Device::class, 'device_id');
    }
    
    
    /**
     * Get the User Who Rents
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'rented_by', 'id');
    }

    
    /**
     * Get the Rental Status of this rental
     */
    public function status()
    {
        return $this->belongsTo(RentalStatus::class, 'rental_status');
    }
}

