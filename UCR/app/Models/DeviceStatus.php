<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeviceStatus extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    /**
    * @var array
    */
    protected $statuses = [
        'NEW', 
        'RENTED', 
        'RETURNED', 
        'MINOR DAMAGED', 
        'MAJOR DAMAGED', 
        'INACTIVE'
    ];

   /**
    * @param int $value
    * @return string|null
    */
    public function getDeviceStatusAttribute($value)
    {
        return Arr::get($this->statuses, $value);
    }
}
