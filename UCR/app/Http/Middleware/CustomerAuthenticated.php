<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerAuthenticated
{
    /**
     * Check if user is authenticated with other roles that is not UCR role (UCR Staff, UCR Manager)
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::check())
        {
            $user = Auth::user();
            // user is Customer then allow to proceed with request.
            if ( $user->isCustomer() ) {
                return $next($request);
            }
            else { 
                abort(403);  // Forbidden = permission denied
            }
        }
        else
        {
            abort(401);  // Unauthorized = aren't authenticated actually
        }
    }
}
