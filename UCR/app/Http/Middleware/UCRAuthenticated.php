<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UCRAuthenticated
{
    /**
     * Check if user is authenticated with UCR role (UCR Staff, UCR Manager)
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::check())
        {
            $user = Auth::user();
            // user is UCR staff/manager then allow to proceed with request
            if ( $user->isUCR() ) {
                return $next($request);
            }
            else { 
                abort(403);  // Forbidden = permission denied
            }
        }
        else
        {
            abort(401);  // Unauthorized = aren't authenticated actually
        }
        
    }
}
