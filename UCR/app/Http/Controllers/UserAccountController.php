<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Device;
use App\Models\Rental;
use App\Models\User;
use App\Models\Account;

class UserAccountController extends Controller
{
    
    /**
     * Common Validator for create/update forms
     *
     * @param  Request  $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(Request $request)
    {
        $data = $request->all();
        return Validator::make($data, [
            'balance' => ['required', 'numeric', 'min:1'],
            'account_no' => ['required', 'string', 'min:16', 'max:16', 'unique:accounts']
        ]);
    }

    /**
     * Return user's account and user's rental history to view.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $account = $user->account;
        
        $rentals = $user->rentals();
        if($rentals !== null)
        {
            $rentals = $rentals->paginate(5);
        }
            
        return view('profile/user_account_summary', compact('account', 'rentals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $account = new Account();
        return view('profile/user_account', compact('account'));
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request);
        if($validator->fails())
        {
            return redirect(url()->previous())
                    ->withErrors($validator)
                    ->withInput();
        }
        $user = Auth::user();
        $data = $request->all();
        Account::create([
            'account_no' => $data['account_no'],
            'balance' => $data['balance'],
            'user_id' => $user->id
        ]);
        
        return redirect()->route('account.index')->with('Info','Success');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account = Account::find($id);
        return view('profile/user_account', compact('account'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $account = Account::find($id);
        $data['balance'] = $account->balance + $data['balance'];
        $account->update($data);
        
        return redirect()->route('account.index')->with('Info','Success');
    }
}
