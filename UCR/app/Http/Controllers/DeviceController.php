<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Device;
use App\Models\DeviceStatus;
use Illuminate\Support\Facades\Auth;
use Exception;

class DeviceController extends Controller
{
    /**
     * Common Validator for create/update forms
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'brand' => 'required',
            'rental_price' => 'required|numeric',
            'serial_no' => 'required',
            'operating_system' => 'required',
            'display_size' => 'required|max:20',
            'ram' => 'required|max:20',
            'usb_ports' => 'required|integer|min:0|max:5',
            'hdmi_ports' => 'required|integer|min:0|max:5'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $devices = Device::paginate(5);
        return view('computers/computers', ['devices' => $devices]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        // user is UCR manager then allow to proceed with request
        if ( $user->isUCRManager() ) {
            $device = new Device();
            return view('computers/computer', compact('device'));
        }
        else { 
            abort(403);  // Forbidden = permission denied
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = $this->validator($input);
        if($validator->fails())
        {
            return redirect(url()->previous())
                    ->withErrors($validator)
                    ->withInput();
        }
        $validator = $request->validate([
            'image_url' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        
        try 
        {
            if($request->hasFile('image_url')){
                $input['image_url'] = $this->uploadImage($request);
            }
        }
        catch (Exception $e) {
            report($e);
        }    
        $device_status = DeviceStatus::where('name', 'NEW')->firstOrFail();
        $input['status_id'] = $device_status->id;
        Device::create($input);

        return redirect()->route('computers.index')->with('Info','Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $device = Device::find($id);
        return view('computers/computer', compact('device'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = $this->validator($input);
        if($validator->fails())
        {
            return redirect(url()->previous())
                    ->withErrors($validator)
                    ->withInput();
        }
        
        if($request->hasFile('image_url')){
            $validator = $request->validate([
                'image_url' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
            
            try 
            {
                $input['image_url'] = $this->uploadImage($request);
            }
            catch (Exception $e) {
                report($e);
            } 
        }
        $device = Device::find($id);
        $device->update($input);
        
        return redirect()->route('computers.index')->with('Info','Success');
    }

    private function uploadImage(Request $request)
    {
        $uploaded_path = 'img/uploaded';
        $image = $request->file('image_url');
        $image_name = $image->getClientOriginalName();
        $image->move(public_path('/' . $uploaded_path),$image_name);
        $uploaded_image_url = $uploaded_path . '/' . $image_name;
        return $uploaded_image_url;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $device = Device::find($id);
        $device->delete();
        return redirect()->route('computers.index')->with('Info','Success');
    }
}
