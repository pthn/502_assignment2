<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Device;
/**
 * For UCR Staff/Manager to manage Rental
 */
class ManageRentalController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'ucr']);
    }

    /**
     * UCR Staff to manage a list of rental devices
     *
     * @return \Illuminate\Http\Response
     */
    public function manage()
    {
        // this joined with rentals table too, it does not only simply retrieve data from devices table.
        $devices = Device::with('rental')->has('rental')->paginate(5);
        return view('rentals/manage_rentals', ['devices' => $devices]);
    }
}
