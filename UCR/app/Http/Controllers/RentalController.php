<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use DB;
use Exception;
use Carbon\Carbon;
use App\Models\Device;
use App\Models\Rental;
use App\Models\RentalStatus;
use App\Models\DeviceStatus;
/**
 * For Customer to rent device
 */
class RentalController extends Controller
{

    public function __construct() {
        // Don't need to check if user login or not
        $this->middleware(['auth', 'customer'], ['except' => [
            'index', 'show', 'search', 'searchByBrand'
        ]]);
    }

    /**
     * Display a listing of available devices for rental.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $devices = Device::whereHas('device_statuses', function($q) {
            $q->whereIn('device_statuses.name', ['NEW', 'RETURNED']);
        })->get();
        return view('rentals/rental_computers', ['devices' => $devices]);
    }

    public function search(Request $request)
    {
        return $this->searchByBrand($request->brand);
    }

    public function searchByBrand($brand)
    {
        $devices = Device::whereRaw('brand = ?', $brand)->whereHas('device_statuses', function($q) {
            $q->whereIn('device_statuses.name', ['NEW', 'RETURNED']);
        })->get();
        return view('rentals/rental_computers', ['devices' => $devices]);
    }

    
    /**
     * Customer rents a device.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'hours' => 'required|numeric|min:1|max:5',
            'start_date' => 'required|date_format:Y-m-d|after_or_equal:today',
            'start_time' => 'required|date_format:H:i|after_or_equal:' . Carbon::now()->format('H:i'),
        ]);
        
        $input = $request->all();
        $device = Device::find($input['device_id']);
        /*
        * Calculating Price to pay for rent and do validation 
        */
        $deposit = Config::get('rental.deposit');
        $rent_per_hours = $input['hours'] * $device->rental_price;
        $min_price_to_pay = $deposit + $rent_per_hours;
        if(!empty($input['insurance']))
        {
            $insurance = Config::get('rental.insurance');
            $min_price_to_pay = $min_price_to_pay + $insurance;
        }
        $validator = $request->validate([
            'balance' => 'numeric|gte:' . $min_price_to_pay,
        ]);
        
        try 
        {
            $rental_status = RentalStatus::where('name', 'RENTED')->firstOrFail();
            $device_status = DeviceStatus::where('name', 'RENTED')->firstOrFail();
            DB::beginTransaction();
            $user = Auth::user();
            $account = $user->account;
            $updated_balance = 0;
            if($account !== null)
            {
                $updated_balance = $account->balance - $min_price_to_pay;
                $account->update([
                    'balance' => $updated_balance
                ]);
            }
            
            Rental::create([
                'start_date' => $input['start_date'] . ' ' . $input['start_time'] . ':00',
                'end_date' =>  $input['start_date'] . ' ' . $input['start_time'] . ':59',
                'rent_period' => $input['hours'],
                'rented_price' => $rent_per_hours,
                'insurance' => $insurance,
                'deposit' => $deposit,
                'rental_status' => $rental_status->id,
                'rented_by' => $user->id,
                'device_id' => $input['device_id']
            ]);

            $device->update([
                'status_id' => $device_status->id,
                'updated_by' => $user->id
            ]);

            DB::commit();
            return $this->index();
        } catch (Exception $e) {
            DB::rollBack();
            report($e);
            abort(500, 'Internal Server Error');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $device = Device::find($id);
        return view('computers/computer_details', compact('device'));
    }

    /**
     * For customers to see a rent page with Device's rental details
     * @param  int  $id Device Id to rent
     * @return \Illuminate\Http\Response
     */
    public function rent($id)
    {
        $device = Device::find($id);
        $user = Auth::user();
        $account = $user->account;
        return view('rentals/rent_computer', compact('device', 'account'));
    }
}
