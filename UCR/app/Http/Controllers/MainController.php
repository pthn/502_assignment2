<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Device;
use App\Models\User;
use App\Models\UserRole;

class MainController extends Controller
{
    public function main()
    {
        $isUCRManager = false;
        $usersByRoles = null;
        $devicesByBrands = null;
        if(Auth::check())
        {
            $user = Auth::user();
            $isUCRManager = $user->isUCRManager();
            if($isUCRManager)
            {
                $usersByRoles = UserRole::where("name", "!=", "UCR_MANAGER")->withCount('users')->get();
                $devicesByBrands = Device::selectRaw('brand, count(*) as brand_count')->groupBy('brand')->get();
            }
        }
        return view('main', compact('isUCRManager', 'usersByRoles', 'devicesByBrands'));
    }
}
