@extends('layouts.app')

@section('content')
@php
    $brands = Config::get('computer.brands');
    $systems = Config::get('computer.systems');
    $title = 'New Device';
@endphp
<main class="mt-4 mb-5">
    <div class="container">
        <div class="row">
            <div class="row pt-1" id="warningDiv">
                <label id="warningLabel" class="text-danger"></label>
            </div>
        </div>
        @if(isset($device->id))
            @php
                $title = 'Edit Device';
            @endphp
            <form id="edit_computer" class="needs-validation" action="{{route('computers.update',$device->id)}}" method="POST" enctype="multipart/form-data">
            @method('PATCH')
        @else
            <form id="add_computer" action="{{route('computers.store')}}" method="POST" enctype="multipart/form-data">
        @endif
            @csrf
            
            <div class="row">
                <div class="col-md-6 pe-4">
                    <h4 class="mb-4">{{ $title }}</h4>
                    <div class="row justify-content-center d-flex align-items-center">
                        <div class="col-md-3">Brand</div>
                        <div class="col-md-9">
                            <select class="form-control" id="brand" name="brand" required>
                                <option value="">Select brand</option>
                                @foreach($brands as $br)
                                    <option value="{{$br}}" {{ (($device->brand === $br) || (old('brand') === $br)) ? 'selected' : ''}}>{{ $br }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row pt-1 justify-content-center d-flex align-items-center">
                        <div class="col-md-3">Rental Price / Hour</div>
                        <div class="col-md-9">
                            <input id="rental_price" type="number" min="1" step="0.01" inputmode="numeric" class="form-control @error('rental_price') is-invalid @enderror" name="rental_price" value="{{ empty($device->rental_price) ? old('rental_price') : $device->rental_price }}" required autocomplete="rental_price">
                            @error('rental_price')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>        
                    </div>
                    <div class="row pt-1 justify-content-center d-flex align-items-center">
                        <div class="col-md-3">Serial No.</div>
                        <div class="col-md-9">
                            <input id="serial_no" type="text" maxlength="20" class="form-control @error('serial_no') is-invalid @enderror" name="serial_no" value="{{ empty($device->serial_no) ? old('serial_no') : $device->serial_no }}" required autocomplete="serial_no">
                            @error('serial_no')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row pt-1 justify-content-center d-flex align-items-center">
                        <div class="col-md-3">Short Description</div>
                        <div class="col-md-9">
                            <textarea name="description" maxlength="100" id="description" class="form-control" style="height: 134px;">{{ empty($device->description) ? old('description') : $device->description }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h4 class="mb-4">Other Details</h4>
                    <div class="row justify-content-center d-flex align-items-center">
                        <div class="col-md-3">Operating System</div>
                        <div class="col-md-9">
                            <select class="form-control" id="operating_system" name="operating_system" required>
                                <option value="">Select OS</option>
                                @foreach($systems as $os)
                                    <option value="{{$os}}" {{ (($device->operating_system === $os) || (old('operating_system') === $os)) ? 'selected' : ''}}>{{ $os }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row justify-content-center d-flex align-items-center pt-1">
                        <div class="col-md-3">Display Size</div>
                        <div class="col-md-9">
                            <input id="display_size" type="text" maxlength="20" class="form-control @error('display_size') is-invalid @enderror" name="display_size" value="{{ empty($device->display_size) ? old('display_size') : $device->display_size }}" required autocomplete="display_size">
                            @error('display_size')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row pt-1 justify-content-center d-flex align-items-center">
                        <div class="col-md-3">RAM</div>
                        <div class="col-md-9">
                            <input id="ram" type="text" maxlength="20" class="form-control @error('ram') is-invalid @enderror" name="ram" value="{{ empty($device->ram) ? old('ram') : $device->ram }}" required autocomplete="ram">
                            @error('ram')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row pt-1 justify-content-center d-flex align-items-center">
                        <div class="col-md-3">USB Ports</div>
                        <div class="col-md-9">
                            <input id="usb_ports" type="number" min="0" max="5" class="form-control @error('usb_ports') is-invalid @enderror" name="usb_ports" value="{{ empty($device->usb_ports) ? old('usb_ports') : $device->usb_ports }}" required autocomplete="usb_ports">
                            @error('usb_ports')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row pt-1 justify-content-center d-flex align-items-center">
                        <div class="col-md-3">HDMI Ports</div>
                        <div class="col-md-9">
                            <input id="hdmi_ports" type="number" min="0" max="5" class="form-control @error('hdmi_ports') is-invalid @enderror" name="hdmi_ports" value="{{ empty($device->hdmi_ports) ? old('hdmi_ports') : $device->hdmi_ports }}" required autocomplete="hdmi_ports">
                            @error('hdmi_ports')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row pt-1 justify-content-center d-flex align-items-center">
                        <div class="col-md-3">Image Path</div>
                        <div class="col-md-6">
                            <input id="image_url" type="file" class="form-control @error('image_url') is-invalid @enderror" name="image_url" max-file-size="2048" value="{{$device->image_url}}" {{ empty($device->image_url) ? 'required' : '' }} >
                            <span>(Max Size: 2mb)</span>
                            @error('image_url')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>
                        <div class="col-md-3">
                            @if(isset($device->image_url) && !empty($device->image_url))
                                <img src="{{ asset($device->image_url) }}" class="img-fluid" style="max-height: 150px;"></div>
                            @endif
                    </div>
                </div>
            </div>    
            <div class="row ps-1 pt-3">
                <div class="col-12 justify-content-center d-flex align-items-center">
                    <input type="hidden" id="id" name="id" value="{{ $device->id }}" />
                    <input id="btnSaveDevice" type="submit" value="Submit" class="btn btn-primary">&nbsp; 
                    <a class="btn btn-secondary" href="{{ route('computers.index') }}">Cancel</a>
                </div>
            </div>
        </form>
    </div>
</main>
@endsection