@extends('layouts.app')

@section('content')
<main class="mt-4 mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-6 pe-4">
            <h4 class="mb-4">Computer Details</h4>
                <div class="row justify-content-center d-flex align-items-center">
                    <div class="col-md-3">Brand</div>
                    <div class="col-md-9">
                    {{$device->brand }}
                    </div>
                </div>
                <div class="row pt-1 justify-content-center d-flex align-items-center">
                    <div class="col-md-3">Base Rental Price / Hour</div>
                    <div class="col-md-9">
                        {{$device->rental_price}}
                    </div>        
                </div>
                <div class="row pt-1">
                    <div class="col-md-8">
                        @if(isset($device->image_url))
                            <img src="{{ asset($device->image_url) }}" class="img-fluid" style="max-height: 150px;">
                        @endif
                    </div>    
                </div>
            </div>
            <div class="col-md-6">
                <h4 class="mb-4">Other Details</h4>
                <div class="row pt-1 justify-content-center d-flex align-items-center">
                    <div class="col-md-3">Serial No.</div>
                    <div class="col-md-9">
                        {{$device->serial_no}}
                    </div>
                </div>
                <div class="row pt-1 justify-content-center d-flex align-items-center">
                    <div class="col-md-3">Operating System</div>
                    <div class="col-md-9">
                        {{ $device->operating_system }}
                    </div>
                </div>
                <div class="row justify-content-center d-flex align-items-center">
                    <div class="col-md-3">Display Size</div>
                    <div class="col-md-9">
                        {{$device->display_size}}
                    </div>
                </div>
                <div class="row pt-1 justify-content-center d-flex align-items-center">
                    <div class="col-md-3">RAM</div>
                    <div class="col-md-9">
                        {{$device->ram}}
                    </div>
                </div>
                <div class="row pt-1 justify-content-center d-flex align-items-center">
                    <div class="col-md-3">USB Ports</div>
                    <div class="col-md-9">
                        {{$device->usb_ports}}
                    </div>
                </div>
                <div class="row pt-1 justify-content-center d-flex align-items-center">
                    <div class="col-md-3">HDMI Ports</div>
                    <div class="col-md-9">
                        {{$device->hdmi_ports}}
                    </div>
                </div>
                <div class="row pt-1 justify-content-center d-flex align-items-center">
                    <div class="col-md-3">Description</div>
                    <div class="col-md-9">
                        {{$device->description}}
                    </div>
                </div>
            </div>
            <div class="row ps-1 pt-3 justify-content-center">
                <a class="col-md-1 btn btn-secondary" href="{{ route('rentals.index') }}">Go Back</a>
            </div>
        </div>
    </div>
</main>
@endsection