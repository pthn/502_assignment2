@extends('layouts.app')

@section('content')
@php
    $is_user_manager = false;
    if (Auth::check())
    {
        $is_user_manager = Auth::user()->isUCRManager();
    }
@endphp
<div class="container" id="page">
    <main class="my-5" id="mainpage">
        <h2>&nbsp; Manage Computers</h2>
        <hr>
        <div class="container">
            @if($is_user_manager)
            <div class="col-md-3 d-flex">
                <a class="btn btn-primary btn-small" id="addComputer" href="{{route('computers.create')}}">Add New Computer</a>
            </div>
            @endif
            
            @if($devices === null || count($devices) === 0)
            <hr>
            <div class="row ps-2 pt-2">
                <h5>There is no computers</h5>
            </div>
            @else
            <div class="row pt-1">
                @foreach ($devices as $device)
                <div class="col-md-12 bg-light mb-2">
                    <div class="card">
                        <div class="row card-body no-spacing">
                            <div class="col-md-2">
                                <img src="{{ asset($device->image_url) }}" class="img-fluid" style="max-height: 150px;">
                            </div>
                            <div class="col-md-4">
                                <h5 class="card-title text-center font-weight-bold">{{ $device->brand }} - {{ $device->device_status->name }} </h5>
                                <div class="row no-spacing" style="display: flex; text-align: left;">
                                    <div class="col-md-7">
                                        <label style="width:50px"><b>Price:</b></label>{{ $device->rental_price }}$/Hour<br/>
                                        <label style="width:50px"><b>OS:</b></label>{{ $device->operating_system }} <br/>
                                    </div>
                                    <div class="col-md-5">
                                        <label style="width:55px"><b>Display:</b></label>{{ $device->display_size }}<br/>
                                        <label style="width:55px"><b>RAM:</b></label>{{ $device->ram }} <br/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <h5 class="card-title text-center font-weight-bold">Other Details</h5>
                                <div class="row no-spacing" style="display: flex; text-align: left;">
                                    <label style="width:60px"><b>S/N:</b></label>{{ $device->serial_no }}<br/>
                                    <label style="width:100px"><b>USB ports:</b></label>{{ $device->usb_ports }}<br/>
                                    <label style="width:100px"><b>HDMI ports:</b></label>{{ $device->hdmi_ports }}<br/>
                                </div>
                                <div class="row no-spacing justify-content-center">
                                    {{ $device->description }}
                                </div>
                            </div>
                            @if($device->device_status->name !== 'RENTED')
                            <div class="col-md-1 pt-1">
                                <div class="row ps-2">
                                    &nbsp;<a class="btn btn-primary btn-small" style="width: 60px;" id="computerdetails" href="{{route('computers.edit',$device->id)}}">Edit</a> <br/>
                                </div>
                                <div class="row pt-2">
                                    <form method="POST" action="{{route('computers.destroy',$device->id)}}">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        
                                        <button type="submit" class="btn btn-danger btn-icon" style="width: 60px;" onclick="return confirm('Are you sure you want to delete this computer?')">
                                            <i class="bi bi-trash3-fill"></i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <!-- Pagination -->
            {{ $devices->links() }}
            @endif
        </div>
    </main>
</div>
@endsection