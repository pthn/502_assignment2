@extends('layouts.app')

@section('content')
<div class="container">
    <main>
        <div class="row">
            @if($isUCRManager !== null && $isUCRManager)
                @include('layouts.manager.main')
            @else
                @include('layouts.customer.main')
            @endif
        </div>    
    </main>
</div>
@endsection