@extends('layouts.app')

@section('content')
@php
    $brands = Config::get('computer.brands');
    $systems = Config::get('computer.systems');
    $is_user_customer = false;
    if (Auth::check())
    {
        $is_user_customer = Auth::user()->isCustomer();
    }
@endphp
<div class="container" id="page">
    <main class="my-5" id="mainpage">
        <h2>&nbsp; Available Computers To Rent</h2>
        <hr>
        <div class="container">
            <nav class="navbar navbar-expand-lg">
                <div class="container-fluid">
                    <div id="navbarSupportedContent" class="row collapse navbar-collapse">
                        <div class="col-md-12 d-flex justify-content-start">
                            <!-- Search form -->
                            <div class="row d-flex justify-content-start">
                                <div class="col-md-12 p5">
                                    @foreach($brands as $brand)
                                    <a class="btn btn-primary btn-small" href="{{route('rentals.searchByBrand',$brand)}}">{{ $brand }}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </nav>
            @if($devices === null || count($devices) === 0)
            <div class="row pt-3">
                <h5>There is no available computers for rent</h5>
            </div>
            @else
            @foreach ($devices->chunk(3) as $chunk)
            <div class="row">
                @foreach ($chunk as $device)        
                <div class="col-md-4 bg-light mb-2">
                    <div class="card">
                        <div class="row card-body no-spacing">
                            <div class="row">
                                <div class="col-md-5">
                                    <img src="{{ asset($device->image_url) }}" class="img-fluid" style="max-height: 200px;">
                                </div>
                                <div class="col-md-7">
                                    <h5 class="card-title text-center font-weight-bold">{{ $device->brand }} </h5>
                                    <div class="row no-spacing" style="display: flex; text-align: left;">
                                        <label style="width:70px"><b>Price:</b></label>{{ $device->rental_price }}$/Hour
                                    </div>
                                    <div class="row no-spacing" style="display: flex; text-align: left;">
                                        <label style="width:70px"><b>OS:</b></label>{{ $device->operating_system }}
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row pt-3 justify-content-center d-flex align-items-center">
                                @if($is_user_customer)
                                <div class="col-md-5">
                                    <a class="btn btn-primary btn-small" id="computerrent" href="{{route('rentals.rent',$device->id)}}">Rent Now</a>
                                </div>
                                @endif
                                <div class="col-md-5">
                                    <a class="btn btn-primary btn-small" id="computerdetails" href="{{route('rentals.show',$device->id)}}">View Details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endforeach
            @endif
        </div>
    </main>
</div>
@endsection