@extends('layouts.app')

@section('content')
@php
    $brands = Config::get('computer.brands');
    $systems = Config::get('computer.systems');
@endphp
<div class="container" id="page">
    <main class="my-5" id="mainpage">
        <h2>&nbsp; Latest Rentals</h2>
        <hr>
        <div class="container">
            @if($devices === null || count($devices) === 0)
            <div class="row ps-2">
                <h5>There is no computers were rented</h5>
            </div>
            @else
            <div class="row">
                @foreach ($devices as $device)
                @php
                    $rental = $device->rental;
                @endphp
                <div class="col-md-12 bg-light mb-2">
                    <div class="card">
                        <div class="row card-body no-spacing">
                            <div class="col-md-2">
                                <img src="{{ asset($device->image_url) }}" class="img-fluid" style="max-height: 100px;">
                            </div>
                            <div class="col-md-4">
                                <h5 class="card-title text-center font-weight-bold">{{ $device->brand }}  - {{ $device->device_status->name }} </h5>
                                <div class="row no-spacing" style="display: flex; text-align: left;">
                                    <div class="col-md-7">
                                        <label style="width:100px"><b>Rented Price:</b></label>{{ $rental->rented_price}}$/Hour<br/>
                                        <label style="width:100px"><b>Deposit:</b></label>{{ empty($rental->deposit) ? "0" : $rental->deposit }}$ <br/>
                                    </div>
                                    <div class="col-md-5">
                                        <label style="width:55px"><b>Period:</b></label>{{ $rental->rent_period }}hr(s)<br/>
                                        <label style="width:55px"><b>By:</b></label>{{ $rental->user->l_name . ' ' . $rental->user->f_name }}  <br/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h5 class="card-title text-center font-weight-bold">Other Details</h5>
                                <div class="row no-spacing" style="display: flex; text-align: left;">
                                    <div class="col-md-6">
                                        <label style="width:55px"><b>Start:</b></label>{{ $rental->start_date }}<br/>
                                        <label style="width:55px"><b>End:</b></label>{{ $rental->end_date }}  <br/>
                                        <label style="width:55px"><b>Return:</b></label>{{ $rental->return_date }}  <br/>
                                    </div>
                                    <div class="col-md-6">
                                        <label style="width:70px"><b>Discount:</b></label>{{ empty($rental->discount) ? "0" : $rental->discount }}$<br/>
                                        <label style="width:70px"><b>Insurance:</b></label>{{ empty($rental->insurance) ? "0" : $rental->insurance }}$<br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <!-- Pagination -->
            {{ $devices->links() }}
            @endif
        </div>
    </main>
</div>
@endsection