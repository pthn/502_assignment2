@extends('layouts.app')

@section('content')
@php
    $brands = Config::get('computer.brands');
    $systems = Config::get('computer.systems');
    $balance = empty($account->balance) ? 0 : $account->balance;
    $deposit = Config::get('rental.deposit');
    $insurance = Config::get('rental.insurance');
    $min_price_to_pay = $deposit + $device->rental_price;
    
@endphp
<div class="container" id="page">
    <main class="my-5" id="mainpage">
        <h2>&nbsp; Rent Computer </h2> 
        <hr>
        <div class="container">
            <form id="rentComputerFrm" action="{{route('rentals.store')}}" method="POST" enctype="multipart/form-data" class="needs-validation">
            @csrf
            <div class="row">
                <div class="col-md-7">
                    <div class="row justify-content-center d-flex align-items-center">
                        <div class="col-md-2"><label for="start_date">{{ __('Rent Date') }}</label></div>
                        <div class="col-md-4">
                            <input id="start_date" type="date" class="form-control @error('start_date') is-invalid @enderror" 
                            name="start_date" value="{{ old('start_date') }}" required autocomplete="start_date" autofocus
                            min="{{ date('Y-m-d') }}">

                            @error('start_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            
                        </div>
                        <div class="col-md-3"><label for="hours">{{ __('Hours') }}</label></div>
                        <div class="col-md-3">
                            <input id="hours" type="number" class="form-control @error('hours') is-invalid @enderror" 
                            name="hours" value="1" required autocomplete="hours" autofocus
                            min="1" max="5" step="0.5">

                            @error('hours')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    
                    <div class="row justify-content-center d-flex align-items-center">
                        <div class="col-md-3">Balance</div>
                        <div class="col-md-3">
                            <input id="balance" type="number" class="form-control @error('balance') is-invalid @enderror" 
                            name="balance" value="{{ $balance }}" readonly step="0.01">

                            @error('balance')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-1">
                <div class="col-md-7">
                    <div class="row justify-content-center d-flex align-items-center">
                        <div class="col-md-2"><label for="start_time">{{ __('Rent Time') }} </label></div>
                        <div class="col-md-4">
                            <input id="start_time" type="time" class="form-control @error('start_time') is-invalid @enderror" 
                            name="start_time" value="{{ old('start_time') }}" required autocomplete="start_time" autofocus
                            min="{{ date('H:i') }}">

                            @error('start_time')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            
                        </div>
                        
                        <div class="col-md-3">Insurance ({{ $insurance }}$)</div>
                        <div class="col-md-3">
                        <input type="checkbox" class="form-check-input" id="insurance" name="insurance" value="{{ $insurance }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row justify-content-center d-flex align-items-center">
                        <div class="col-md-3">Base Price</div>
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="rental_price" name="rental_price" value="{{ $device->rental_price }}" disabled>
                        </div>
                    </div>
                    <div class="row justify-content-center d-flex align-items-center pt-1">
                        <div class="col-md-3">Deposit Fee</div>
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="deposit" name="deposit" value="{{ $deposit }}" disabled>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="row ps-1 pt-3 justify-content-center d-flex align-items-center">
                <div class="col-md-3">
                <input type="hidden" name="device_id" value="{{ $device->id }}">
                    @if(!empty($account->balance) && $account->balance >= $min_price_to_pay )
                    <button type="submit" class="btn btn-primary float-right"> Submit </button>
                    @else
                    <span class="text-danger">You do not have sufficient balance to rent.</span>
                    @endif
                    <a class="btn btn-secondary" href="{{ route('rentals.index') }}">Cancel</a>
                </div>
            </div>
            </form>
        </div>
    </main>
</div>
@endsection