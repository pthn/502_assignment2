@extends('layouts.app')

@section('content')
@php
    $title = 'Create New Account';
    $balance_title = 'Balance';
@endphp
<div class="container" id="page">
    <main class="my-5" id="mainpage">
        <div class="container">
            <div class="row justify-content-center">
                @if(isset($account->id))
                    @php
                        $title = 'Deposit Account Balance';
                        $balance_title = 'Top-up Balance';
                    @endphp
                    <form id="update_account" class="needs-validation" action="{{route('account.update',$account->id)}}" method="POST" enctype="multipart/form-data">
                    @method('PATCH')
                @else
                    <form id="add_account" action="{{route('account.store')}}" method="POST" enctype="multipart/form-data">
                @endif
                    @csrf
                <h4 class="mb-4">{{ $title }}</h4>
                                
                <div class="col-md-12 bg-light mb-2 rounded">
                    <div class="card">
                    
                            <div class="row card-body">
                                <div class="row mb-3">
                                     <label for="account_no" class="col-md-4 col-form-label text-md-end">Account No</label>
                                    <div class="col-md-3">
                                        @if(isset($account->id))
                                        <label class="col-form-label text-md-end">{{ $account->account_no }}</label>    
                                        @else
                                        <input id="account_no" type="text" 
                                            class="form-control @error('account_no') is-invalid @enderror" name="account_no" 
                                            value="{{ $account->account_no }}" required autocomplete="account_no" autofocus
                                            pattern="[A-Za-z0-9]{16}" title="Account number must contain 16 digits without space">

                                            @error('account_no')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        
                                        @endif    
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="balance" class="col-md-4 col-form-label text-md-end">{{ $balance_title }}</label>

                                    <div class="col-md-3">
                                        <input id="balance" type="number" class="form-control @error('balance') is-invalid @enderror" 
                                        name="balance" value="1" required autocomplete="balance" autofocus
                                        min="1" step="0.01"> 
                                        @error('balance')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    @if($account->id !== null)
                                        <label class="col-md-3 col-form-label text-md-end">( Current Balance: {{ ($account->balance === null) ? '0' : $account->balance}}$ )</label>
                                    @endif
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-2 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Submit') }}
                                        </button>
                                        <a class="btn btn-secondary" href="{{ route('account.index') }}">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection