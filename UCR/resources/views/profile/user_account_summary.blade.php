@extends('layouts.app')

@section('content')
<div class="container" id="page">
    <div class="container">
        <div class="row">
            <h2>Account</h2>
            <hr>
            @if($account !== null)
            <div class="col-md-3 mb-2">
                <a class="btn btn-primary btn-small" href="{{route('account.edit',$account->id)}}">Deposit Fund</a>
            </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12 bg-light mb-2">
                <div class="card">
                    <div class="row ps-1">
                    @if($account === null)
                    <h5>You do not have an account</h5>
                    <div class="col-md-3 mb-2 pt-4">
                        <a class="btn btn-primary btn-small"  href="{{route('account.create')}}">Create New</a> 
                    </div>
                    @else
                    <div class="col-md-4 mb-2">
                        <label class="fw-bold">Account No:</label> {{ $account->account_no}}
                    </div>
                    <div class="col-md-4 mb-2">
                    <label class="fw-bold">Balance:</label> {{ ($account->balance === null) ? '0' : $account->balance}}$
                    </div>
                    @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row pt-5">
            <h2>Rental History</h2>
            <hr>
            @if($rentals === null || count($rentals) === 0)
                <div class="col-md-12 bg-light mb-2">
                    <h5>You have no Rental</h5>
                </div>    
            @else
                <div class="row ps-3 fw-bold">
                    <div class="col-md-3">Brand / Status</div>
                    <div class="col-md-1">Period</div>
                    <div class="col-md-1">Price</div>
                    <div class="col-md-1">Deposit</div>
                    <div class="col-md-2">Insurance/Discount</div>
                    <div class="col-md-2">&nbsp;Rent Date</div>
                    <div class="col-md-2">Return Date</div>
                </div>
                @foreach ($rentals as $rental)
                <div class="col-md-12 bg-light mb-2">
                    <div class="card">
                        <div class="row card-body no-spacing">
                            <div class="col-md-3">{{ $rental->device->brand }} - {{ $rental->status->name }}</div>
                            <div class="col-md-1">{{ $rental->rent_period }} hr(s)</div>
                            <div class="col-md-1">{{ ($rental->rented_price === null) ? '0' : $rental->rented_price }}$</div>
                            <div class="col-md-1">{{ ($rental->deposit === null) ? '0' : $rental->deposit }}$</div>
                            <div class="col-md-2">{{ ($rental->insurance === null) ? '0' : $rental->insurance}}$ / {{ ($rental->discount === null) ? '0' : $rental->discount}}</div>
                            <div class="col-md-2">{{ $rental->start_date }}</div>
                            <div class="col-md-2">{{ ($rental->return_date === null) ? 'N/A' : $rental->return_date }}</div>
                            
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- Pagination -->
                {{ $rentals->links() }}
            @endif    
        </div>
    </div>
</div>
@endsection