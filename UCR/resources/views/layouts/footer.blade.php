<footer class="text-center text-white" style="background-color: #163a5a;">
    <div class="container p-4 pb-0">
        <!-- UCR Social Media -->
        <section class="mb-4">
        <a class="btn btn-outline-light btn-floating m-1" href="https://www.facebook.com/UniversityofTasmania/" role="button">
            <i class="bi bi-facebook"></i>
        </a>
        <a class="btn btn-outline-light btn-floating m-1" href="https://www.instagram.com/universityoftasmania/?hl=en" role="button">
            <i class="bi bi-twitter"></i>
        </a>
        </section>
    </div>

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
        © 2022 Copyright:
        <a class="text-white">UCR - UTAS Computer Rental Services</a>
    </div>
    <!-- Copyright -->
</footer>