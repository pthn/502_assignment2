@php
    // set it = true by default, as unauthenticated user can also see the Rental page
    $is_user_customer = true;
    $is_user_ucr = false;

    if (Auth::check())
    {
        $is_user_ucr = Auth::user()->isUCR();
        $is_user_customer = Auth::user()->isCustomer();
    }
    $brands = Config::get('computer.brands');
@endphp
<!-- Navigation Menu-->
<header>
    <div class="row" id="header">    
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" id="ucrindex" href="{{ route('main.home') }}"><label class="brand strong">UCR</label></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarMenu" 
                aria-controls="navbarMenu" aria-expanded="false">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarMenu">
                    <ul class="navbar-nav me-auto">
                    @if($is_user_ucr)
                       <li class="nav-item">
                            <a class="nav-link {{request()->is('rentals', 'rentals/*') ? 'active' : ''}}" id="managerentalpage" href="{{ route('rentals.manage') }}">Manage Rental </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('computers', 'computers/*') ? 'active' : ''}}" id="computerpage" href="{{ route('computers.index') }}">Manage Computers </a>
                        </li>
                    @endif
                    
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('rentals', 'rentals/*') ? 'active' : ''}}" id="rentalpage" href="{{ route('rentals.index') }}">Rent Computer</a>
                        </li>
                        @if(!Request::is('register') and !Request::is('login'))
                        <li class="nav-item">
                            <form id="search_devices" action="{{route('rentals.search')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                                <div class="input-group" style="padding-left: 50px;">
                                    <select class="form-control me-2 custom-select" name="brand">
                                        <option value="">Select Brand</option>
                                        @foreach($brands as $brand)
                                            <option value="{{$brand}}">{{ $brand }}</option>
                                        @endforeach
                                    </select>
                                    <div class="input-group-append">
                                        <input id="btnSearchDevices" type="submit" value="Search" class="btn btn-primary">
                                    </div>
                                </div>
                            </form>
                        </li>
                        @endif
                    
                    </ul>
                    
                    
                    <div class="d-flex" id="credentials">
                    @guest
                    @if(!Request::is('register') and !Request::is('login'))
                        <a href="{{ route('login') }}" class="btn btn-primary nav-item-button">Sign in</a>
                        <a href="{{ route('register') }}" class="btn btn-blue nav-item-button">Sign Up</a>
                    @endif
                    @else
                        <div class="dropdown pe-5">
                            <i class="bi bi-person-fill" id="myaccount"></i> {{ Auth::user()->f_name . ' ' . Auth::user()->l_name }}
                                <div class="dropdown-content">
                                  <a href="{{ route('account.index') }}">My Account</a>
                                  <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                        </div>
                    @endguest
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>