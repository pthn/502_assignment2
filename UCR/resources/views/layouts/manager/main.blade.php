<div class="row d-flex justify-content-center">
    <div class="col-md-8 bg-light mb-2">
        <div class="card">
            <div class="row card-body no-spacing">
            <h5 class="card-title text-center fw-bold pt-2"> Users By Groups Statistic </h5>
                @if($usersByRoles === null || count($usersByRoles) === 0)
                    <h7>There is no other user groups in the system yet.</h7>
                @else
                    <div class="row d-flex justify-content-center fw-bold">
                        <div class="col-md-8 ps-5">
                            <h7>User Group</h7>
                        </div>
                        <div class="col-md-4 text-center">
                            <h7>Total Users</h7>
                        </div>
                    </div>
                    <div class="row ps-4">
                        <hr>
                    </div>
                    @foreach ($usersByRoles as $usersByRole)
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-8 ps-5">
                                <label>{{ $usersByRole->name }}</label>
                            </div>
                            <div class="col-md-4 text-center">
                                <label>{{ $usersByRole->users_count }}</label>
                            </div>
                        </div>
                    @endforeach    
                @endif
            </div>
        </div>
    </div>    
</div>
<div class="row d-flex justify-content-center pt-5">
    <div class="col-md-8 bg-light mb-2">
        <div class="card">
            <div class="row card-body no-spacing">
            <h5 class="card-title text-center fw-bold pt-2"> Computers By Brands Statistic </h5>
                @if($devicesByBrands === null || count($devicesByBrands) === 0)
                    <h7>There is no other computers in the system yet.</h7>
                @else
                    <div class="row d-flex justify-content-center fw-bold">
                        <div class="col-md-8 ps-5">
                            <h7>Brand</h7>
                        </div>
                        <div class="col-md-4 text-center">
                            <h7>Total Computers</h7>
                        </div>
                    </div>
                    <div class="row ps-4">
                        <hr>
                    </div>
                    @foreach ($devicesByBrands as $devicesByBrand)
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-8 ps-5">
                                <label>{{ $devicesByBrand->brand }}</label>
                            </div>
                            <div class="col-md-4 text-center">
                                <label>{{ $devicesByBrand->brand_count }}</label>
                            </div>
                        </div>
                    @endforeach    
                @endif
            </div>
        </div>
    </div>    
</div>