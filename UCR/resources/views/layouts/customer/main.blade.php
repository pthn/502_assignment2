
<!-- Carousel-->
<div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-inner">
    <div class="carousel-item active">
        <div class="row d-block w-100" style="height: 80vh;">
            <img src="{{ asset('img/pexels-photo-2569997.jpeg') }}" class="d-block w-100">
        </div>
        <div class="carousel-caption d-none d-md-block text-center">
            <h5>Convenient</h5>
            <p>Online Rental Services make it easy</p>
        </div>
    </div>
    <div class="carousel-item">
        <div class="row d-block w-100" style="height: 80vh;">
            <img src="{{ asset('img/pexels-tatiana-syrikova-3975590.jpg') }}" class="d-block w-100">
        </div>
        <div class="carousel-caption d-none d-md-block text-center">
            <h5>Availability</h5>
            <p>Wide range of laptop devices for your choice</p>
        </div>
    </div>
    <div class="carousel-item">
        <div class="row d-block w-100" style="height: 80vh;">
            <img src="{{ asset('img/pexels-vlada-karpovich-4050334.jpg') }}"  class="d-block w-100">
        </div>
        <div class="carousel-caption d-none d-md-block text-center">
            <h5>Discount</h5>
            <p>Affordable for Student </p>
        </div>
    </div>
    </div>  
</div>