<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
@include('layouts.head')
</head>
<body class="bg-light" style="overflow-x: hidden;">
    @include('layouts.header')
    
    <div class="container">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
    @yield('jscript')
</body>
@include('layouts.footer')
</html>
