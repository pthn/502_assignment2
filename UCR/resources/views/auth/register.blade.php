@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 bg-light rounded">
            <div class="card">
                <div class="card-header"><h3 class="font-bold">Create a new account</h3></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" name="registrationFrm" class="needs-validation">
                        @csrf

                        <div class="row mb-3">
                            <label for="f_name" class="col-md-4 col-form-label text-md-end">{{ __('First Name') }}</label>

                            <div class="col-md-6">
                                <input id="f_name" type="text" class="form-control @error('f_name') is-invalid @enderror" name="f_name" value="{{ old('f_name') }}" required autocomplete="f_name" autofocus>

                                @error('f_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="l_name" class="col-md-4 col-form-label text-md-end">{{ __('Last Name') }}</label>

                            <div class="col-md-6">
                                <input id="l_name" type="text" class="form-control @error('l_name') is-invalid @enderror" name="l_name" value="{{ old('l_name') }}" required autocomplete="l_name" autofocus>

                                @error('l_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="phone_number" class="col-md-4 col-form-label text-md-end">{{ __('Phone Number') }}</label>

                            <div class="col-md-6">
                                <input id="phone_number" type="text" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') }}" required autocomplete="phone_number" autofocus
                                    pattern="([0-9]){8,12}" title="Phone number must contain 8-12 numbers without space">

                                @error('phone_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" 
                                pattern="^($|^.*@[^@\s]+\.[^@\s]+$)">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password"
                                pattern="^(?=.*[0-9])(?=.*?[!@#$*])((?=.*[a-z])(?=.*[A-Z]).{5,10})$" title="Password must contain 5 - 10 characters with at least
                                                     1 lowercase letter, 1 uppercase letter, 1 number and one of the following
                                                    special characters such as !, @, #, or $">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                        <div class="row pt-1" id="warning" style="display: none;">
                            <label id="warning" class="text-danger">Confirmed Password must match Password</label>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-2 no-spacing offset-md-5">
                                <input type="radio" name="role" class="radio-middle" value="STUDENT" checked> Student
                            </div>
                            <div class="col-md-5">
                                <input type="radio" name="role" value="STAFF" class="radio-middle"> Staff
                            </div>
                        </div>

                        <div class="row pt-4">
                            <div class="col-md-2 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Sign Up') }}
                                </button>
                            </div>
                            <div class="col-md-5">
                                Already have an account? <a href="{{ route('login') }}">Log In now</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('jscript')
<script type="text/javascript">
    $(document).ready(function(){
        alert("CHECK");
        $("#registrationFrm").submit(function() {
            alert("CHECK FRM");
            var passwordVal = $("#password").val();
            var confirmPasswordVal = $("#password-confirm").val();
            alert(passwordVal === confirmPasswordVal);
            if(passwordVal === confirmPasswordVal)
            {
                // or to call back-end service here
                $("#warning").hide();
                return true;
            }
            else 
            {
                $("#warning").show();
                return false;
            }
        });
    }
</script>
@endsection