<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
| All Route Resources were created by using 'php artisan make:controller <ControllerName> --resource' command.
| Reason: These route controllers all have basic CRUD actions.
| See: https://laravel.com/docs/9.x/controllers (Resource Controllers) as best practice from Laravel
*/

Route::get('/', [App\Http\Controllers\MainController::class, 'main'])->name('main.home');

Route::resource('computers', 'App\Http\Controllers\DeviceController')->middleware(['auth', 'ucr']);

Route::get('rentals/manage',  [App\Http\Controllers\ManageRentalController::class, 'manage'])->name('rentals.manage');

Route::post('rentals/search', 'App\Http\Controllers\RentalController@search')->name('rentals.search');
Route::get('rentals/search/{brand}', 'App\Http\Controllers\RentalController@searchByBrand')->name('rentals.searchByBrand');
Route::get('rentals/rent/{id}', 'App\Http\Controllers\RentalController@rent')->middleware(['auth', 'customer'])->name('rentals.rent');
Route::resource('rentals', 'App\Http\Controllers\RentalController');

Route::resource('account', 'App\Http\Controllers\UserAccountController')->middleware('auth');

Auth::routes();

