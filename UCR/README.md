## General Purpose
This ReadMe file instructs Lecturer or Tutor on:
1. How to access the web application - URL link here: 
https://ictteach-www.its.utas.edu.au/pthn/assignmentApp/public/index.php

2. Which Laravel app on UserMin to get the source code for the web app:
/home/pthn/public_html/assignmentApp

3. Where to access Database:
https://ictteach-www.its.utas.edu.au/pthn/phpliteadmin/index.php?database=.%2Fdb%2FUCR

4. Test Users created:
You could use the following users account to login:
    Password: pass123456
is the same for all users below.

a. UCR Manager user:
Email: ucr.m.a@utas.edu.au

b. UCR Staff user:
Email: ucr.st.a@utas.edu.au

c. UTAS Staff user:
Email: st.a@utas.edu.au

d. Student users:
Email: s.a@utas.edu.au
Email: s.b@utas.edu.au

This ReadMe file also guides Lecture or Tutor to understand the implementation of the application:
1. Scope of the features/pages are excluded as instructed by Dr. Soonja as I am working alone.
2. A word file 'UCR_Implementation_Guideline.docx' attached in the Submitted folder uploaded to MyLo (and also under Usermin's "/home/pthn/public_html/Assignments/502_assignment2_group89..."), to describe:
- Code changes/New files I created
- Screenshots to explain each screen and what each does.

3. Authorization Process
4. References of Images used.


## Features To Be Reduced
As per discussion with Dr. Soonja, following features are reduced as I am working alone in this project.

1. Rental Page:
[Reduced: deposit, insurance, pending payment and discount supports are not required]

2. [Reduced: Returning the Rental page is not required]

3. A manage rental page (only available to staff and web manager): 
[Reduced: confirming computer returns and updating conditions/damages on returned computers are not required]

4. [Reduced: User Management Page is not required]

5. A manager dashboard (only available to web manager):
[Reduced: Information about blacklisted customers and rental status of computers are not required]

## APP Configurations
NOTE: 
1. All default application's configurations (from Assignment's Specification) are set at app\Providers\AppServiceProvider.php.
2. The pagination is current set at 5 records per page, in all controllers.

## Commands
1. Commands for generating Laravel Auth as per instruction below are to showcase how I did.
There is no need for Lecturer or Tutor to execute any of any commands.

2. Database Commands:
Database Migrate and Database Seeder Commands ... which related to Database migration and initialize data through 'db:seeder' are not required to be run by Lecturer or Tutor or anyone.

The files (under 'database\seeders') are used by me to initialize some data that are used for the Application. They were ran once by me during setup, and that one run is enough.
+ App's default data such as: device statuses, rental statuses, user roles.
+ Test data (these are just creating some front data for smoother use of the application, especially when displaying Device List, without requiring users to input so much data for testing).
 

## Laravel Authorization
NOTE: 
Below are steps to explain how to auto-generate and modify custom Laravel Auth with Authorization. 
There is no need for Lecturer or Tutor to execute any of these steps below.

*------------------------*
Following are 2 commands I used to create two Middleware: UCRAuthenticated and CustomerAuthenticated
php artisan make:middleware UCRAuthenticated
php artisan make:middleware CustomerAuthenticated

---
## References 
#### Images Used In This Web Application
    The images used in this web application is free to download.
    Free Images are downloaded from:
	https://www.pexels.com/photo/silver-and-black-dell-laptop-beside-white-calla-lily-in-clear-glass-vase-on-brown-wooden-desk-1266982/
	https://www.pexels.com/photo/laptop-2569997/
    https://www.pexels.com/photo/anonymous-woman-using-laptop-in-bedroom-3975590/
	https://www.pexels.com/photo/content-young-woman-using-laptop-in-modern-living-room-4050334/
    https://www.pexels.com/photo/macbook-pro-and-space-gray-iphone-6-303383/
    https://www.pexels.com/photo/macbook-on-a-table-4006151/
    https://www.pexels.com/photo/macbook-pro-turned-off-205421/
    https://www.pexels.com/photo/silver-and-black-dell-laptop-beside-white-calla-lily-in-clear-glass-vase-on-brown-wooden-desk-1266982/
    https://www.pexels.com/photo/person-working-remotely-3987066/
    https://www.pexels.com/photo/photo-of-a-gray-dell-laptop-displaying-pexels-webpage-811587
    https://www.pexels.com/photo/acer-silver-and-black-laptop-169484
    https://www.pexels.com/photo/close-up-of-camera-330160/
    https://www.pexels.com/photo/top-view-of-white-acer-laptop-with-black-keyboard-6452/
    https://www.pexels.com/photo/black-and-silver-laptop-computer-3550482/ 
	https://www.pexels.com/photo/photo-of-a-gray-dell-laptop-displaying-pexels-webpage-811587/
	https://www.pexels.com/photo/acer-silver-and-black-laptop-169484/
#### Content Copyright

---
